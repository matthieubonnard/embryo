install
text
reboot
url --mirrorlist=http://mirrors.fedoraproject.org/mirrorlist?repo=fedora-$releasever&arch=$basearch
lang fr_FR.UTF-8
keyboard fr
timezone --utc Etc/UTC
rootpw --plaintext packer
user --name=packer --groups=packer --password=packer --plaintext
services --enabled=network
zerombr
autopart --type=plain
clearpart --all --initlabel
bootloader --extlinux --timeout=1

%packages --excludedocs --nobase
@core
vim-minimal
NetworkManager
%end

%post --erroronfail
# Warning : This take long time to execute
# dnf -y update

cat <<EOF > /etc/sudoers.d/packer
Defaults:packer !requiretty
packer ALL=(ALL) NOPASSWD: ALL
EOF
chmod 440 /etc/sudoers.d/packer

%end
